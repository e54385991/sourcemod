@ECHO OFF 
echo Starting Build SM

xcopy *.* D:\alliedmodders\sourcemod\ /s /e /c /y /h /r

::xcopy D:\VS2017BuildFix\*.* D:\alliedmodders\sourcemod\ /s /e /c /y /h /r

echo Starting Build Sourcemod

call "C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Auxiliary/Build/vcvars32.bat"

rd /s /q D:\alliedmodders\sourcemod\build
mkdir D:\alliedmodders\sourcemod\build
cd /d D:\alliedmodders\sourcemod\build
python ../configure.py --enable-optimize --no-mysql --sdks csgo
ambuild

exit
